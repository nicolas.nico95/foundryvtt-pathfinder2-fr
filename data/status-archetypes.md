# État de la traduction (archetypes)

 * **changé**: 75
 * **aucune**: 7
 * **libre**: 3


Dernière mise à jour: 2021-11-18 07:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[2utUBjuTmPt7cHiw.htm](archetypes/2utUBjuTmPt7cHiw.htm)|Ghost Eater|
|[7G7cRmS3HO2iUElx.htm](archetypes/7G7cRmS3HO2iUElx.htm)|Artillerist|
|[ARb0Q9D2gmqalPnM.htm](archetypes/ARb0Q9D2gmqalPnM.htm)|Beast Gunner|
|[CmCmbpn10LJcjH78.htm](archetypes/CmCmbpn10LJcjH78.htm)|Sixth Pillar|
|[O9aKJ2C3MvHKFmcO.htm](archetypes/O9aKJ2C3MvHKFmcO.htm)|Bullet Dancer|
|[pgesyqPUJSHKsbGv.htm](archetypes/pgesyqPUJSHKsbGv.htm)|Butterfly Blade|
|[S82AsQp9Iyjff12F.htm](archetypes/S82AsQp9Iyjff12F.htm)|Golden League Xun|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[25NJcH5SkWh1Tol7.htm](archetypes/25NJcH5SkWh1Tol7.htm)|Spellmaster|Maître des sorts|changé|
|[2eAiIynDWTMKTGDU.htm](archetypes/2eAiIynDWTMKTGDU.htm)|Scrollmaster|Maître des parchemins|changé|
|[2TRqXfTtfMhTSIDY.htm](archetypes/2TRqXfTtfMhTSIDY.htm)|Ghost Hunter|Chasseur de Fantômes|changé|
|[2Wol7CpOBJjVT3aP.htm](archetypes/2Wol7CpOBJjVT3aP.htm)|Hellknight|Chevalier infernal|changé|
|[4xzvjFVytFA0LIYj.htm](archetypes/4xzvjFVytFA0LIYj.htm)|Living Monolith|Monolithe vivant|changé|
|[5cbEwgPLlMWGGVxN.htm](archetypes/5cbEwgPLlMWGGVxN.htm)|Dandy|Dandy|changé|
|[5zILvODoiQC34tWU.htm](archetypes/5zILvODoiQC34tWU.htm)|Blessed One|Élu divin|changé|
|[678FjGfCmxuNbNhe.htm](archetypes/678FjGfCmxuNbNhe.htm)|Lion Blade|Lame du Lion|changé|
|[70DYBk9gZCH9uSUs.htm](archetypes/70DYBk9gZCH9uSUs.htm)|Shadowdancer|Danseur de l'ombre|changé|
|[7BNWl18efHJ93hke.htm](archetypes/7BNWl18efHJ93hke.htm)|Assassin|Assassin|changé|
|[8vuL2hCPrcG2duLt.htm](archetypes/8vuL2hCPrcG2duLt.htm)|Juggler|Jongleur|changé|
|[akmaoP0StWvIv3jb.htm](archetypes/akmaoP0StWvIv3jb.htm)|Oracle|Oracle|changé|
|[bDP7kO6bnQh6no0C.htm](archetypes/bDP7kO6bnQh6no0C.htm)|Bright Lion|Lion Radieux|changé|
|[bi1SeBS7Af3ZisIA.htm](archetypes/bi1SeBS7Af3ZisIA.htm)|Swordmaster|Maître des épées|changé|
|[Bl0KM29OOlwBegCi.htm](archetypes/Bl0KM29OOlwBegCi.htm)|Familiar Master|Maître familier|changé|
|[Buptz08MArCtY41w.htm](archetypes/Buptz08MArCtY41w.htm)|Investigator|Enquêteur|changé|
|[cEiMI4QGqjv69pJ6.htm](archetypes/cEiMI4QGqjv69pJ6.htm)|Herbalist|Herboriste|changé|
|[cnsq5cXgQu4FXnoZ.htm](archetypes/cnsq5cXgQu4FXnoZ.htm)|Aldori Duelist|Duelliste Aldori|changé|
|[DeixUOfwL7Bruad5.htm](archetypes/DeixUOfwL7Bruad5.htm)|Red Mantis Assassin|Assassin des Mantes rouges|changé|
|[DMsE43xgf1gHPl8o.htm](archetypes/DMsE43xgf1gHPl8o.htm)|Loremaster|Maître savant|changé|
|[DYJfhMcLlP05oMDD.htm](archetypes/DYJfhMcLlP05oMDD.htm)|Duelist|Duelliste|changé|
|[e2KCqgD3zt8yvxGg.htm](archetypes/e2KCqgD3zt8yvxGg.htm)|Sentinel|Sentinelle|changé|
|[eEyxTQ3oHfthdg0e.htm](archetypes/eEyxTQ3oHfthdg0e.htm)|Scrounger|Bricoleur|changé|
|[fjyB6HdS95xHEQn0.htm](archetypes/fjyB6HdS95xHEQn0.htm)|Poisoner|Empoisonneur|changé|
|[GADKhvTRWCqlI9vy.htm](archetypes/GADKhvTRWCqlI9vy.htm)|Oozemorph|Vasemorphe|changé|
|[GCoO342NLbrmEer1.htm](archetypes/GCoO342NLbrmEer1.htm)|Bounty Hunter|Chasseur de primes|changé|
|[GQn5HfP1Jg5qO0Dz.htm](archetypes/GQn5HfP1Jg5qO0Dz.htm)|Swashbuckler|Bretteur|changé|
|[gw4J1pXYED71TPWx.htm](archetypes/gw4J1pXYED71TPWx.htm)|Bellflower Tiller|Laboureur du Campanule|changé|
|[GwItivMACzBmW82g.htm](archetypes/GwItivMACzBmW82g.htm)|Hellknight Signifer|Signifer|changé|
|[hLFBAs2NpyGjQiDA.htm](archetypes/hLFBAs2NpyGjQiDA.htm)|Cavalier|Cavalier|changé|
|[IdhIKSuKueYt6RsQ.htm](archetypes/IdhIKSuKueYt6RsQ.htm)|Eldritch Researcher|Chercheur mystique|changé|
|[IoDeWTPMoa7LNLWD.htm](archetypes/IoDeWTPMoa7LNLWD.htm)|Ritualist|Ritualiste|changé|
|[IU0WKnP9qciXztOC.htm](archetypes/IU0WKnP9qciXztOC.htm)|Gladiator|Gladiateur|changé|
|[JiRqSMv1STETLwSP.htm](archetypes/JiRqSMv1STETLwSP.htm)|Student of Perfection|Étudiant en Perfection|changé|
|[K1mmFJ3QSjNkHm2L.htm](archetypes/K1mmFJ3QSjNkHm2L.htm)|Dual-Weapon Warrior|Combattant à deux armes|changé|
|[kisnuKPMGheORROU.htm](archetypes/kisnuKPMGheORROU.htm)|Animal Trainer|Dompteur|changé|
|[l4sKdl0ub0UGBwsb.htm](archetypes/l4sKdl0ub0UGBwsb.htm)|Knight Vigilant|Chevalier vigilant|changé|
|[Ld3wizivrctmoHhy.htm](archetypes/Ld3wizivrctmoHhy.htm)|Martial Artist|Artiste martial|changé|
|[Lr108TWazbuxxUzP.htm](archetypes/Lr108TWazbuxxUzP.htm)|Zephyr Guard|Garde Zéphyr|changé|
|[lsU4NQEei111OetM.htm](archetypes/lsU4NQEei111OetM.htm)|Dragon Disciple|Disciple draconique|changé|
|[LYYDozZfXok19VYW.htm](archetypes/LYYDozZfXok19VYW.htm)|Hellknight Armiger|Écuyer des chevaliers infernaux|changé|
|[MJ2EBeAVkc61mznR.htm](archetypes/MJ2EBeAVkc61mznR.htm)|Archer|Archer|changé|
|[myvFp45VnA2FPOvG.htm](archetypes/myvFp45VnA2FPOvG.htm)|Archaeologist|Archéologue|changé|
|[NGrKVIaZJx3r3In4.htm](archetypes/NGrKVIaZJx3r3In4.htm)|Weapon Improviser|Improvisateur d'arme|changé|
|[o3C85NHW08Y2inAN.htm](archetypes/o3C85NHW08Y2inAN.htm)|Pathfinder Agent|Agent des Éclaireurs|changé|
|[OmEG0nhQgeKnAUsn.htm](archetypes/OmEG0nhQgeKnAUsn.htm)|Magic Warrior|Guerrier magique|changé|
|[pHTnfVHOzsgfZ9E2.htm](archetypes/pHTnfVHOzsgfZ9E2.htm)|Scout|Éclaireur|changé|
|[PlPc4w1XG9VOpDgn.htm](archetypes/PlPc4w1XG9VOpDgn.htm)|Pirate|Pirate|changé|
|[Q3VydQ6b5Mxm1c41.htm](archetypes/Q3VydQ6b5Mxm1c41.htm)|Edgewatch Detective|Officier d'Edgewatch|changé|
|[Qsmzm6w1ZNpHX7GD.htm](archetypes/Qsmzm6w1ZNpHX7GD.htm)|Halcyon Speaker|Orateur paisible|changé|
|[R0ORjHAeQ0auxl2j.htm](archetypes/R0ORjHAeQ0auxl2j.htm)|Vigilante|Justicier|changé|
|[RvjlaAPa2Wkdhrms.htm](archetypes/RvjlaAPa2Wkdhrms.htm)|Horizon Walker|Arpenteur d'horizon|changé|
|[RyjrUkqSIg52Mjl8.htm](archetypes/RyjrUkqSIg52Mjl8.htm)|Runescarred|Scarifié des runes|changé|
|[s7kWIaeCdj3IFte9.htm](archetypes/s7kWIaeCdj3IFte9.htm)|Jalmeri Heavenseeker|Chercheur de paradis du Jalmeray|changé|
|[SAyPnVRucJa8k4MN.htm](archetypes/SAyPnVRucJa8k4MN.htm)|Witch|Sorcière|changé|
|[SbvhfCo5gFVt1DgD.htm](archetypes/SbvhfCo5gFVt1DgD.htm)|Acrobat|Acrobate|changé|
|[SJi2cicLd7R2FInl.htm](archetypes/SJi2cicLd7R2FInl.htm)|Viking|Viking|changé|
|[TdoPhja8JA9C2Itm.htm](archetypes/TdoPhja8JA9C2Itm.htm)|Linguist|Linguiste|changé|
|[ThxMkphglPJuweXB.htm](archetypes/ThxMkphglPJuweXB.htm)|Celebrity|Célébrité|changé|
|[uKoay1d62vRae3z1.htm](archetypes/uKoay1d62vRae3z1.htm)|Snarecrafter|Fabricant de pièges artisanaux|changé|
|[UOhVrWvuaAVtQHEo.htm](archetypes/UOhVrWvuaAVtQHEo.htm)|Mauler|Cogneur|changé|
|[UsT8XqN5LwfbBMzp.htm](archetypes/UsT8XqN5LwfbBMzp.htm)|Bastion|Bastion|changé|
|[Utxq3fo8AoPyfh4K.htm](archetypes/Utxq3fo8AoPyfh4K.htm)|Lastwall Sentry|Sentinelle du Dernier-Rempart|changé|
|[VRUYc3QyRjjOjppc.htm](archetypes/VRUYc3QyRjjOjppc.htm)|Turpin Rowe Lumberjack|Bûcheron de Turpin Rowe|changé|
|[WrQ2Qpv958C6cfD0.htm](archetypes/WrQ2Qpv958C6cfD0.htm)|Knight Reclaimant|Chevalier reconquérant|changé|
|[XATPzPTZ9Je89erT.htm](archetypes/XATPzPTZ9Je89erT.htm)|Golem Grafter|Greffeur de golem|changé|
|[xFjXKdIkA574QRkv.htm](archetypes/xFjXKdIkA574QRkv.htm)|Scroll Trickster|Usurpateur de parchemins|changé|
|[xNzNpPjeNSrXE7WG.htm](archetypes/xNzNpPjeNSrXE7WG.htm)|Crystal Keeper|Gardien des cristaux|changé|
|[XZwGLLgC1sIKlR1c.htm](archetypes/XZwGLLgC1sIKlR1c.htm)|Staff Acrobat|Funambule|changé|
|[YBvXmJ5e9MThdws0.htm](archetypes/YBvXmJ5e9MThdws0.htm)|Firebrand Braggart|Agitateur vantard|changé|
|[YJQJxskQf8VqDt8G.htm](archetypes/YJQJxskQf8VqDt8G.htm)|Provocator|Provocator|changé|
|[yvb2pY3Qb7Jl35hk.htm](archetypes/yvb2pY3Qb7Jl35hk.htm)|Magaambyan Attendant|Gardien du Magaambya|changé|
|[z5yvyY2HfIRgwGnO.htm](archetypes/z5yvyY2HfIRgwGnO.htm)|Eldritch Archer|Archer mystique|changé|
|[zv31F34JgUesREuz.htm](archetypes/zv31F34JgUesREuz.htm)|Marshal|Marshal|changé|
|[ZvIreFl9TXgrj52Q.htm](archetypes/ZvIreFl9TXgrj52Q.htm)|Beastmaster|Maître des bêtes|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[3jceS1GfWcSQ3tMY.htm](archetypes/3jceS1GfWcSQ3tMY.htm)|Talisman Dabbler|Amateur de talismans|libre|
|[CA22ZhzFPjahrO4W.htm](archetypes/CA22ZhzFPjahrO4W.htm)|Medic|Médecin|libre|
|[PPmO6FXrl8Otszji.htm](archetypes/PPmO6FXrl8Otszji.htm)|Drow Shootist|Tireur drow|libre|
