# État de la traduction (ancestries)

 * **libre**: 21
 * **changé**: 1
 * **officielle**: 9


Dernière mise à jour: 2021-11-18 07:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[6F2fSFC1Eo1JdpY4.htm](ancestries/6F2fSFC1Eo1JdpY4.htm)|Poppet|Poupée|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[18xDKYPDBLEv2myX.htm](ancestries/18xDKYPDBLEv2myX.htm)|Tengu|Tengu|libre|
|[4BL5wf1VF9feC2rY.htm](ancestries/4BL5wf1VF9feC2rY.htm)|Kitsune|Kitsune|libre|
|[7oQxL6wgsokD3QXG.htm](ancestries/7oQxL6wgsokD3QXG.htm)|Kobold|Kobold|libre|
|[972EkpJOPv9KkQIW.htm](ancestries/972EkpJOPv9KkQIW.htm)|Catfolk|Homme-félin (félide)|libre|
|[BYj5ZvlXZdpaEgA6.htm](ancestries/BYj5ZvlXZdpaEgA6.htm)|Dwarf|Nain|officielle|
|[c4secsSNG2AO7I5i.htm](ancestries/c4secsSNG2AO7I5i.htm)|Goloma|Goloma|libre|
|[cdhgByGG1WtuaK73.htm](ancestries/cdhgByGG1WtuaK73.htm)|Leshy|Léchi|officielle|
|[CYlfsYLJcBOgqKtD.htm](ancestries/CYlfsYLJcBOgqKtD.htm)|Gnome|Gnome|officielle|
|[FXlXmNBFiiz9oasi.htm](ancestries/FXlXmNBFiiz9oasi.htm)|Fleshwarp|Distordu|libre|
|[GfLwE884NoRC7cRi.htm](ancestries/GfLwE884NoRC7cRi.htm)|Android|Androïde|libre|
|[GgZAHbrjnzWOZy2v.htm](ancestries/GgZAHbrjnzWOZy2v.htm)|Halfling|Halfelin|officielle|
|[GXcC6oVa5quzgNHD.htm](ancestries/GXcC6oVa5quzgNHD.htm)|Strix|Strix|libre|
|[hIA3qiUsxvLZXrFP.htm](ancestries/hIA3qiUsxvLZXrFP.htm)|Fetchling|Fetchelin|libre|
|[HWEgF7Gmoq55VhTL.htm](ancestries/HWEgF7Gmoq55VhTL.htm)|Lizardfolk|Hommes-lézard|officielle|
|[hXM5jXezIki1cMI2.htm](ancestries/hXM5jXezIki1cMI2.htm)|Grippli|Grippli|libre|
|[IiG7DgeLWYrSNXuX.htm](ancestries/IiG7DgeLWYrSNXuX.htm)|Human|Humain|officielle|
|[kYsBAJ103T44agJF.htm](ancestries/kYsBAJ103T44agJF.htm)|Automaton|Automate|libre|
|[lSGWXjcbOa6O5fTx.htm](ancestries/lSGWXjcbOa6O5fTx.htm)|Orc|Orc|libre|
|[mDtlRBBUWqUzYSHs.htm](ancestries/mDtlRBBUWqUzYSHs.htm)|[Empty Ancestry]|[Ascendance vierge]|libre|
|[P6PcVnCkh4XMdefw.htm](ancestries/P6PcVnCkh4XMdefw.htm)|Ratfolk|Homme-rat|libre|
|[PgKmsA2aKdbLU6O0.htm](ancestries/PgKmsA2aKdbLU6O0.htm)|Elf|Elfe|officielle|
|[piNLXUrm9iaGqD2i.htm](ancestries/piNLXUrm9iaGqD2i.htm)|Hobgoblin|Hobgobelin|officielle|
|[q6rsqYARyOGXZA8F.htm](ancestries/q6rsqYARyOGXZA8F.htm)|Shoony|Shoony|libre|
|[sQfjTMDaZbT9DThq.htm](ancestries/sQfjTMDaZbT9DThq.htm)|Goblin|Gobelin|officielle|
|[TQEqWqc7BYiadUdY.htm](ancestries/TQEqWqc7BYiadUdY.htm)|Anadi|Anadi|libre|
|[TRqoeYfGAFjQbviF.htm](ancestries/TRqoeYfGAFjQbviF.htm)|Sprite|Sprite|libre|
|[tZn4qIHCUA6wCdnI.htm](ancestries/tZn4qIHCUA6wCdnI.htm)|Conrasu|Conrasu|libre|
|[vxbQ1Yw4qwgjTzqo.htm](ancestries/vxbQ1Yw4qwgjTzqo.htm)|Gnoll|Gnoll|libre|
|[x1YinOddgUxwOLqP.htm](ancestries/x1YinOddgUxwOLqP.htm)|Shisk|Shisk|libre|
|[yFoojz6q3ZjvceFw.htm](ancestries/yFoojz6q3ZjvceFw.htm)|Azarketi|Azarketi|libre|
