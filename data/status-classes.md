# État de la traduction (classes)

 * **officielle**: 1
 * **changé**: 17
 * **libre**: 3


Dernière mise à jour: 2021-11-18 07:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[30qVs46dVNflgQNx.htm](classes/30qVs46dVNflgQNx.htm)|Inventor|Inventeur|changé|
|[3gweRQ5gn7szIWAv.htm](classes/3gweRQ5gn7szIWAv.htm)|Bard|Barde|changé|
|[7s57JDCaiYYCAdFx.htm](classes/7s57JDCaiYYCAdFx.htm)|Druid|Druide|changé|
|[8zn3cD6GSmoo1LW4.htm](classes/8zn3cD6GSmoo1LW4.htm)|Fighter|Guerrier|changé|
|[bYDXk9HUMKOuym9h.htm](classes/bYDXk9HUMKOuym9h.htm)|Witch|Sorcière|changé|
|[EizrWvUPMS67Pahd.htm](classes/EizrWvUPMS67Pahd.htm)|Cleric|Prêtre|changé|
|[HQBA9Yx2s8ycvz3C.htm](classes/HQBA9Yx2s8ycvz3C.htm)|Magus|Magus|changé|
|[LO9STvskJemPkiAI.htm](classes/LO9STvskJemPkiAI.htm)|Rogue|Roublard|changé|
|[pWHx4SXcft9O2udP.htm](classes/pWHx4SXcft9O2udP.htm)|Oracle|Oracle|changé|
|[RwjIZzIxzPpUglnK.htm](classes/RwjIZzIxzPpUglnK.htm)|Wizard|Magicien|changé|
|[uJ5aCzlw34GGdWjp.htm](classes/uJ5aCzlw34GGdWjp.htm)|Swashbuckler|Bretteur|changé|
|[x8iwnpdLbfcoZkHA.htm](classes/x8iwnpdLbfcoZkHA.htm)|Champion|Champion|changé|
|[XwfcJuskrhI9GIjX.htm](classes/XwfcJuskrhI9GIjX.htm)|Alchemist|Alchimiste|changé|
|[YDRiP7uVvr9WRhOI.htm](classes/YDRiP7uVvr9WRhOI.htm)|Barbarian|Barbare|changé|
|[Yix76sfxrIlltSTJ.htm](classes/Yix76sfxrIlltSTJ.htm)|Ranger|Rôdeur|changé|
|[YPxpk9JbMnKjbNLc.htm](classes/YPxpk9JbMnKjbNLc.htm)|Monk|Moine|changé|
|[YtOm245r8GFSFYeD.htm](classes/YtOm245r8GFSFYeD.htm)|Summoner|Conjurateur|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[15Yc1r6s9CEhSTMe.htm](classes/15Yc1r6s9CEhSTMe.htm)|Sorcerer|Ensorceleur|officielle|
|[4wrSCyX6akmyo7Wj.htm](classes/4wrSCyX6akmyo7Wj.htm)|Investigator|Enquêteur|libre|
|[84SZGVNLXhLKG6gL.htm](classes/84SZGVNLXhLKG6gL.htm)|[Empty Class]|_Classe vierge|libre|
|[Z9li154CPNmun29Q.htm](classes/Z9li154CPNmun29Q.htm)|Gunslinger|Franc-tireur|libre|
