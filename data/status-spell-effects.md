# État de la traduction (spell-effects)

 * **changé**: 220
 * **libre**: 92
 * **aucune**: 36


Dernière mise à jour: 2021-11-18 07:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[01-9ZIP6gWSp9OTEu8i.htm](spell-effects/01-9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|
|[01-jKrLpG74nLHYGyWB.htm](spell-effects/01-jKrLpG74nLHYGyWB.htm)|Spell Effect: Nudge Fate|
|[01-pocsoEi84Mr2buOc.htm](spell-effects/01-pocsoEi84Mr2buOc.htm)|Spell Effect: Evolution Surge (Scent)|
|[02-buXx8Azr4BYWPtFg.htm](spell-effects/02-buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|
|[02-HoOujAdQWCN4E6sQ.htm](spell-effects/02-HoOujAdQWCN4E6sQ.htm)|Spell Effect: Barkskin|
|[02-HtaDbgTIzdiTiKLX.htm](spell-effects/02-HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|
|[03-6BjslHgY01cNbKp5.htm](spell-effects/03-6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|
|[03-E6HPkpvzvkaJ49c0.htm](spell-effects/03-E6HPkpvzvkaJ49c0.htm)|Spell Effect: Elemental Absorption (Earth)|
|[03-gX8O0ArQXbEVDUbW.htm](spell-effects/03-gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|
|[03-n3gkkiOcLcSHxAah.htm](spell-effects/03-n3gkkiOcLcSHxAah.htm)|Spell Effect: Elemental Absorption (Water)|
|[03-nfQAqTSVHcA2JSyI.htm](spell-effects/03-nfQAqTSVHcA2JSyI.htm)|Spell Effect: Elemental Absorption (Air)|
|[03-VAJMojQATvpAcpQW.htm](spell-effects/03-VAJMojQATvpAcpQW.htm)|Spell Effect: Elemental Absorption (Fire)|
|[04-0IxfOSTMen1GKQUW.htm](spell-effects/04-0IxfOSTMen1GKQUW.htm)|Spell Effect: Energy Absorption (Electricity)|
|[04-5xCheSMgtQhQZm00.htm](spell-effects/04-5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|
|[04-8wCVSzWYcURWewbd.htm](spell-effects/04-8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|
|[04-9K0QeW8j0Z1WswnZ.htm](spell-effects/04-9K0QeW8j0Z1WswnZ.htm)|Spell Effect: Adaptive Ablation (Electricity)|
|[04-cNZRAPd2kVrM3Iuo.htm](spell-effects/04-cNZRAPd2kVrM3Iuo.htm)|Spell Effect: Energy Absorption (Acid)|
|[04-db9lXpF4OsPz9cXL.htm](spell-effects/04-db9lXpF4OsPz9cXL.htm)|Spell Effect: Energy Absorption (Fire)|
|[04-GlggmEqkGVj1noOD.htm](spell-effects/04-GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|
|[04-KtAJN4Qr2poTL6BB.htm](spell-effects/04-KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|
|[04-LMXxICrByo7XZ3Q3.htm](spell-effects/04-LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|
|[04-NhauZhE2kQKw8MRx.htm](spell-effects/04-NhauZhE2kQKw8MRx.htm)|Spell Effect: Adaptive Ablation (Fire)|
|[04-pgzrKgfIgf2a36Qy.htm](spell-effects/04-pgzrKgfIgf2a36Qy.htm)|Spell Effect: Adaptive Ablation (Acid)|
|[04-vYSS2W2BA0SiY5yL.htm](spell-effects/04-vYSS2W2BA0SiY5yL.htm)|Spell Effect: Adaptive Ablation (Cold)|
|[04-WquoEAIT621zDLpY.htm](spell-effects/04-WquoEAIT621zDLpY.htm)|Spell Effect: Energy Absorption (Cold)|
|[04-ZHVtJKnur9PAF5TO.htm](spell-effects/04-ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|
|[04-ZlsuhS9J0S3PuvCO.htm](spell-effects/04-ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Blink|
|[04-ZXWDzLJ8583Z4hOD.htm](spell-effects/04-ZXWDzLJ8583Z4hOD.htm)|Spell Effect: Adaptive Ablation (Sonic)|
|[05-Dr7KMMtE4twxO6Ks.htm](spell-effects/05-Dr7KMMtE4twxO6Ks.htm)|Spell Effect: Blessing of Defiance (2 Action Fortitude)|
|[05-kDx9XcyWm4KirRPY.htm](spell-effects/05-kDx9XcyWm4KirRPY.htm)|Spell Effect: Blessing of Defiance (2 Action Will)|
|[06-nemThuhp3praALY6.htm](spell-effects/06-nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|
|[07-4Lo2qb5PmavSsLNk.htm](spell-effects/07-4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|
|[07-LT5AV9vSN3T9x3J9.htm](spell-effects/07-LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|
|[08-DBaMtFHRPEg1JeLs.htm](spell-effects/08-DBaMtFHRPEg1JeLs.htm)|Spell Effect: Mind Blank|
|[09-xKJVqN1ETnNH3tFg.htm](spell-effects/09-xKJVqN1ETnNH3tFg.htm)|Spell Effect: Corrosive Body (Heightened 9th)|
|[18-eotqxEWIgaK7nMpD.htm](spell-effects/18-eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00-Gqy7K6FnbLtwGpud.htm](spell-effects/00-Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Effet : Bénédiction|changé|
|[01-06zdFoxzuTpPPGyJ.htm](spell-effects/01-06zdFoxzuTpPPGyJ.htm)|Effect: Rejuvinating Flames|Effet: Flammes rajeunissantes|changé|
|[01-0gv9D5RlrF5cKA3I.htm](spell-effects/01-0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|Effet : Adaptation de soi|changé|
|[01-0R42NyuEZMVALjQs.htm](spell-effects/01-0R42NyuEZMVALjQs.htm)|Spell Effect: Traveler's Transit (Swim)|Effet : Voyageur en transit (Nage)|changé|
|[01-1kelGCsoXyGRqMd9.htm](spell-effects/01-1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|Effet: Édit diabolique|changé|
|[01-2SWUzp4JuNK5EX0J.htm](spell-effects/01-2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|Effet : Adaptation de soi (Nage)|changé|
|[01-3Ktyd5F9lOPo4myk.htm](spell-effects/01-3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|Effet : Déguisement illusoire|changé|
|[01-3LyOkV25p7wA181H.htm](spell-effects/01-3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|Effet: Immunité à Assistance divine|changé|
|[01-3qHKBDF7lrHw8jFK.htm](spell-effects/01-3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Effet : Assistance divine|changé|
|[01-4CUZSDn4BiqWpvZg.htm](spell-effects/01-4CUZSDn4BiqWpvZg.htm)|Spell Effect: Ki Strike (Air)|Effet : Frappe ki (Air)|changé|
|[01-4iakL7fDcZ8RT6Tu.htm](spell-effects/01-4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|Effet : Fondu dans la foule|changé|
|[01-57lnrCzGUcNUBP2O.htm](spell-effects/01-57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|Effet : Athlétisme poussé|changé|
|[01-6embuvXCpS3YOD5u.htm](spell-effects/01-6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|Effet : Toucher de résilience|changé|
|[01-8adLKKzJy49USYJt.htm](spell-effects/01-8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|Chanson de force|changé|
|[01-8XaSpienzVXLmcfp.htm](spell-effects/01-8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|Effet : Inspiration héroïque (Force, +3)|changé|
|[01-a5rWrWwuevTzs9Io.htm](spell-effects/01-a5rWrWwuevTzs9Io.htm)|Spell Effect: Wild Shape|Effet: Morphologie sauvage|changé|
|[01-aDOL3OAEWf3ka9oT.htm](spell-effects/01-aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|Effet: Protection du sang|changé|
|[01-alyNtkHLNnt98Ewz.htm](spell-effects/01-alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|Effet : Contact accélérant|changé|
|[01-an4yZ6dyIDOFa1wa.htm](spell-effects/01-an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|Effet : Paroles apaisantes|changé|
|[01-B6zSwNFJV3TMafHf.htm](spell-effects/01-B6zSwNFJV3TMafHf.htm)|Spell Effect: Protection from Lawful|Effet : Protection du Loyal|changé|
|[01-Bc2Bwuan3716eAyY.htm](spell-effects/01-Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|Effet : Source de sérénité|changé|
|[01-beReeFroAx24hj83.htm](spell-effects/01-beReeFroAx24hj83.htm)|Spell Effect: Inspire Courage|Effet : Inspiration Vaillante|changé|
|[01-BKam63zT98iWMJH7.htm](spell-effects/01-BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|Effet : Inspiration héroïque (Défense, +3)|changé|
|[01-BQAb290GJQHiXtSW.htm](spell-effects/01-BQAb290GJQHiXtSW.htm)|Spell Effect: Ki Strike (Positive)|Effet : Frappe ki (Positif)|changé|
|[01-ccrcaKIRxiCnnRPT.htm](spell-effects/01-ccrcaKIRxiCnnRPT.htm)|Spell Effect: Dragon Claw (Poison)|Effet: Griffes de dragon (Poison)|changé|
|[01-Chol7ExtoN2T36mP.htm](spell-effects/01-Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|Effet : Inspiration héroïque (Défense, +2)|changé|
|[01-ctMxYPGEpstvhW9C.htm](spell-effects/01-ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|Effet : Sceau d'interdiction|changé|
|[01-d0jsFEx8kq9idSlo.htm](spell-effects/01-d0jsFEx8kq9idSlo.htm)|Spell Effect: Boost Eidolon (3 Dice)|Effet: Booster l'eidolon (3 Dés)|changé|
|[01-deG1dtfuQph03Kkg.htm](spell-effects/01-deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|Effet : Gourdin magique|changé|
|[01-dEsaufFnfYihu5Ex.htm](spell-effects/01-dEsaufFnfYihu5Ex.htm)|Spell Effect: Discern Secrets (Sense Motive)|Effet: Discerner les secrets (Deviner les intentions)|changé|
|[01-DLwTvjjnqs2sNGuG.htm](spell-effects/01-DLwTvjjnqs2sNGuG.htm)|Spell Effect: Inspire Defense|Effet : Inspiration défensive|changé|
|[01-dWbg2gACxMkSnZag.htm](spell-effects/01-dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|Effet : Champ protecteur|changé|
|[01-eFxUcQDkxcX9KoE1.htm](spell-effects/01-eFxUcQDkxcX9KoE1.htm)|Spell Effect: Boost Eidolon (1 Die)|Effet: Booster l'eidolon (1 Dé)|changé|
|[01-Fjnm1l59KH5YJ7G9.htm](spell-effects/01-Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|Effet : Inspiration héroïque (Force, +2)|changé|
|[01-GhNVAYtoF5hK3AlD.htm](spell-effects/01-GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of Corruption|Effet : Toucher de corruption|changé|
|[01-GnWkI3T3LYRlm3X8.htm](spell-effects/01-GnWkI3T3LYRlm3X8.htm)|Spell Effect: Magic Weapon|Effet : Arme magique|changé|
|[01-hdOb5Iu6Zd3pHoGI.htm](spell-effects/01-hdOb5Iu6Zd3pHoGI.htm)|Spell Effect: Discern Secrets (Recall Knowledge)|Effet: Discerner les secrets (Se souvenir)|changé|
|[01-hqe0hdGH1hvhcZyz.htm](spell-effects/01-hqe0hdGH1hvhcZyz.htm)|Spell Effect: Wild Morph (Wings)|Effet : Métamorphose sauvage (Ailes)|changé|
|[01-I4PsUAaYSUJ8pwKC.htm](spell-effects/01-I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|Effet : Rayon de givre|changé|
|[01-J60rN48XzBGHmR6m.htm](spell-effects/01-J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|Effet: Incarnation élémentaire (Air)|changé|
|[01-Jemq5UknGdMO7b73.htm](spell-effects/01-Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Effet : Bouclier|changé|
|[01-JJ7HZkXXm8sTapcu.htm](spell-effects/01-JJ7HZkXXm8sTapcu.htm)|Spell Effect: Boost Eidolon (4 Dice)|Effet: Booster l'eidolon (4 Dés)|changé|
|[01-jp88SCE3VCRAyE6x.htm](spell-effects/01-jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|Effet : Incarnation élémentaire (Terre)|changé|
|[01-KNWXmArZ82el4VUv.htm](spell-effects/01-KNWXmArZ82el4VUv.htm)|Spell Effect: Dragon Claw (Acid)|Effet: Griffes de dragon (Acide)|changé|
|[01-kZ39XWJA3RBDTnqG.htm](spell-effects/01-kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|Effet : Inspiration héroïque (Courage, +2)|changé|
|[01-lmAwCy7isFvLYdGd.htm](spell-effects/01-lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|Effet: Incarnation élémentaire (Feu)|changé|
|[01-lpqkZkslFMirsItL.htm](spell-effects/01-lpqkZkslFMirsItL.htm)|Spell Effect: Protection from Chaotic|Effet : Protection du Chaos|changé|
|[01-lyLMiauxIVUM3oF1.htm](spell-effects/01-lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|Effet : Imposition des mains|changé|
|[01-m1tQTBrolf7uZBW0.htm](spell-effects/01-m1tQTBrolf7uZBW0.htm)|Spell Effect: Discern Secrets (Seek)|Effet: Discerner les secrets (Chercher)|changé|
|[01-MJSoRFfEdM4j5mNG.htm](spell-effects/01-MJSoRFfEdM4j5mNG.htm)|Spell Effect: Sweet Dream (Glamour)|Effet : Doux rêve (Glamour)|changé|
|[01-MjtPtndJx31q2N9R.htm](spell-effects/01-MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|Effet : Toucher amplificateur|changé|
|[01-MKOg6feUPgCHyBja.htm](spell-effects/01-MKOg6feUPgCHyBja.htm)|Spell Effect: Dragon Claw (Electricity)|Effet: Griffes de dragon (Électricité)|changé|
|[01-mrSulUdNbwzGSwfu.htm](spell-effects/01-mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaw|Effet: Mâchoires gloutonnes|changé|
|[01-N1EM3jRyT8PCG1Py.htm](spell-effects/01-N1EM3jRyT8PCG1Py.htm)|Spell Effect: Traveler's Transit (Climb)|Effet : Voyageur en transit (Escalade)|changé|
|[01-ND1Gfo1yJZ87Ia34.htm](spell-effects/01-ND1Gfo1yJZ87Ia34.htm)|Spell Effect: Wild Morph (Wild Jaws)|Effet : Morphologie sauvage (Mâchoires)|changé|
|[01-nEqBnsrvJZzh8Bil.htm](spell-effects/01-nEqBnsrvJZzh8Bil.htm)|Spell Effect: Protection from Evil|Effet : Protection du Mal|changé|
|[01-nkk4O5fyzrC0057i.htm](spell-effects/01-nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|Effet : Apaiser|changé|
|[01-oaRt210JV4GZIHmJ.htm](spell-effects/01-oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|Effet : Toucher rajeunissant|changé|
|[01-oi5M6yQBlTKvLy29.htm](spell-effects/01-oi5M6yQBlTKvLy29.htm)|Spell Effect: Gravity Weapon (3 Dice)|Effet : Arme pesante (3 dés)|changé|
|[01-Pfllo68qdQjC4Qv6.htm](spell-effects/01-Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|Effet: Bouclier prismatique|changé|
|[01-PNEGSVYhMKf6kQZ6.htm](spell-effects/01-PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|Effet : Appel aux armes|changé|
|[01-ppVKJY6AYggn2Fma.htm](spell-effects/01-ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|Effet : Baie nourricière|changé|
|[01-PQHP7Oph3BQX1GhF.htm](spell-effects/01-PQHP7Oph3BQX1GhF.htm)|Spell Effect: Longstrider|Effet : Grande foulée|changé|
|[01-pUYL0ILL3r8VBFm0.htm](spell-effects/01-pUYL0ILL3r8VBFm0.htm)|Spell Effect: Dragon Claw (Cold)|Effet: Griffes de dragon (Froid)|changé|
|[01-qhNUfwpkD8BRw4zj.htm](spell-effects/01-qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|Effet : Peau magique|changé|
|[01-qkwb5DD3zmKwvbk0.htm](spell-effects/01-qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mage Armor|Effet : Armure du mage|changé|
|[01-qlz0sJIvqc0FdUdr.htm](spell-effects/01-qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|Effet : Arme améliorée|changé|
|[01-qQLHPbUFASKFky1W.htm](spell-effects/01-qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|Effet : Hyperfocalisation|changé|
|[01-QrULQZsSRGSa6qOt.htm](spell-effects/01-QrULQZsSRGSa6qOt.htm)|Spell Effect: Dragon Claw (Fire)|Effet: Griffes de dragon (Feu)|changé|
|[01-qVngSDWvHcnht0fP.htm](spell-effects/01-qVngSDWvHcnht0fP.htm)|Spell Effect: Protection from Good|Effet : Protection du Bien|changé|
|[01-rnu7tqMPe3Qm0apj.htm](spell-effects/01-rnu7tqMPe3Qm0apj.htm)|Spell Effect: Ki Strike (Fire)|Effet : Frappe ki (Feu)|changé|
|[01-s6CwkSsMDGfUmotn.htm](spell-effects/01-s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|Effet : Protection contre la mort|changé|
|[01-sE2txm68yZSFMV3v.htm](spell-effects/01-sE2txm68yZSFMV3v.htm)|Spell Effect: Sweet Dream (Voyaging)|Effet : Doux rêve (Voyage)|changé|
|[01-Tdcv2q9zlXZBZRMT.htm](spell-effects/01-Tdcv2q9zlXZBZRMT.htm)|Spell Effect: Wild Morph (Wild Claws)|Effet : Morphologie sauvage (Griffes)|changé|
|[01-ThFug45WHkQQXcoF.htm](spell-effects/01-ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|Effet: Pas rapide|changé|
|[01-tNryw0Rl4L8LF8Yq.htm](spell-effects/01-tNryw0Rl4L8LF8Yq.htm)|Spell Effect: Gravity Weapon (4 Dice)|Effet : Arme pesante (4 dés)|changé|
|[01-TpVkVALUBrBQjULn.htm](spell-effects/01-TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|Effet: Enflammer les cœurs|changé|
|[01-tTRy1cc24ApPnym8.htm](spell-effects/01-tTRy1cc24ApPnym8.htm)|Spell Effect: Ki Strike (Water)|Effet : Frappe ki (Eau)|changé|
|[01-TwtUIEyenrtAbeiX.htm](spell-effects/01-TwtUIEyenrtAbeiX.htm)|Spell Effect: Tanglefoot|Effet : Entrave|changé|
|[01-U2dJnkoGSCYOsQFI.htm](spell-effects/01-U2dJnkoGSCYOsQFI.htm)|Spell Effect: Gravity Weapon (1 Die)|Effet : Arme pesante (1 dé)|changé|
|[01-U2ekk5iBaSfZlEkp.htm](spell-effects/01-U2ekk5iBaSfZlEkp.htm)|Spell Effect: Ki Strike (Earth)|Effet : Frappe ki (Terre)|changé|
|[01-ubT5UGReu1PoeCL0.htm](spell-effects/01-ubT5UGReu1PoeCL0.htm)|Spell Effect: Ki Strike (Negative)|Effet : Frappe ki (Négatif)|changé|
|[01-uDOxq24S7IT2EcXv.htm](spell-effects/01-uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|Effet : Mémoire de l'objet (arme ou outil)|changé|
|[01-uJNDI3CGGOtcG8nB.htm](spell-effects/01-uJNDI3CGGOtcG8nB.htm)|Spell Effect: Gravity Weapon (2 Dice)|Effet : Arme pesante (2 dés)|changé|
|[01-UtIOWubq7akdHMOh.htm](spell-effects/01-UtIOWubq7akdHMOh.htm)|Spell Effect: Augment Summoning|Effet: Convocation améliorée|changé|
|[01-UTLp7omqsiC36bso.htm](spell-effects/01-UTLp7omqsiC36bso.htm)|Spell Effect: Bane|Effet : Imprécation|changé|
|[01-v051JKN0Dj3ve5cF.htm](spell-effects/01-v051JKN0Dj3ve5cF.htm)|Spell Effect: Sweet Dream (Insight)|Effet : Doux rêve (Intuition)|changé|
|[01-VFereWC1agrwgzPL.htm](spell-effects/01-VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|Effet : Inspiration héroïque (Courage, +3)|changé|
|[01-vLdt7vdUPv3Jdkgq.htm](spell-effects/01-vLdt7vdUPv3Jdkgq.htm)|Spell Effect: Ki Strike (Force)|Effet : Frappe ki (Force)|changé|
|[01-W0PjCMyGOpKAuyKX.htm](spell-effects/01-W0PjCMyGOpKAuyKX.htm)|Spell Effect: Weapon Surge (Major Striking)|Effet : Arme améliorée (Frappe majeure)|changé|
|[01-w1HwO7huxJoK0gHY.htm](spell-effects/01-w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|Effet: Incarnation élémentaire (Eau)|changé|
|[01-X2BGmuKdGVj4YYyp.htm](spell-effects/01-X2BGmuKdGVj4YYyp.htm)|Spell Effect: Ki Strike (Lawful)|Effet : Frappe ki (Loi)|changé|
|[01-X7RD0JRxhJV9u2LC.htm](spell-effects/01-X7RD0JRxhJV9u2LC.htm)|Spell Effect: Disrupting Weapons|Effet : Armes perturbatrices|changé|
|[01-xFvNsjmhvTkVk59U.htm](spell-effects/01-xFvNsjmhvTkVk59U.htm)|Spell Effect: Boost Eidolon (2 Dice)|Effet: Booster l'eidolon (2 Dés)|changé|
|[01-XMBoKRRyooKnGkHk.htm](spell-effects/01-XMBoKRRyooKnGkHk.htm)|Spell Effect: Practise Makes Perfect|Effet : En forgeant on devient forgeron|changé|
|[01-XT3AyRfx4xeXfAjP.htm](spell-effects/01-XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|Effet: Amélioration physique|changé|
|[01-y9PJdDYFemhk6Z5o.htm](spell-effects/01-y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|Effet : Pieds Agiles|changé|
|[01-ydsLEGjY89Akc4oZ.htm](spell-effects/01-ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|Effet : Forme de nuisible|changé|
|[01-zjFN1cJEl3AMKiVs.htm](spell-effects/01-zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|Effet: Amulette de la nymphe|changé|
|[01-zpxIwEjnLUSO1B4z.htm](spell-effects/01-zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|Effet : Réceptacle magique|changé|
|[02-2wfrhRLmmgPSKbAZ.htm](spell-effects/02-2wfrhRLmmgPSKbAZ.htm)|Spell Effect: Animal Feature (Claws)|Effet : Trait animal (Griffes)|changé|
|[02-4U9BWv0rSpGbU0wr.htm](spell-effects/02-4U9BWv0rSpGbU0wr.htm)|Spell Effect: Ki Strike (Evil)|Effet : Frappe ki (Mal)|changé|
|[02-70qdCBokXBvKIUIQ.htm](spell-effects/02-70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|Effet : Vision de faiblesse|changé|
|[02-BT1ofB6RvRocQOWO.htm](spell-effects/02-BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|Effet : Forme animale (Taureau)|changé|
|[02-dCQCzapIk53xmDo5.htm](spell-effects/02-dCQCzapIk53xmDo5.htm)|Spell Effect: Animal Feature (Cat Eyes)|Effet : Trait animal (Yeux de chats)|changé|
|[02-F4DTpDXNu5IliyhJ.htm](spell-effects/02-F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|Effet : Forme animale (Cerf)|changé|
|[02-GhGoZdAZtzZTYCzj.htm](spell-effects/02-GhGoZdAZtzZTYCzj.htm)|Spell Effect: Animal Feature (Jaws)|Effet : Trait animal (Mâchoires)|changé|
|[02-gQnDKDeBTtjwOWAk.htm](spell-effects/02-gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|Effet : Forme animale (Ours)|changé|
|[02-j2LhQ7kEQhq3J3zZ.htm](spell-effects/02-j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|Effet : Forme animale (Grenouille)|changé|
|[02-joCQGq27FylDuY9P.htm](spell-effects/02-joCQGq27FylDuY9P.htm)|Spell Effect: Ki Strike (Chaotic)|Effet : Frappe ki (Chaotique)|changé|
|[02-kz3mlFwb9tV9bFwu.htm](spell-effects/02-kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|Effet : Forme animale (Serpent)|changé|
|[02-LXf1Cqi1zyo4DaLv.htm](spell-effects/02-LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|Effet : Rétrécir|changé|
|[02-mr6mlkUMeStdChxi.htm](spell-effects/02-mr6mlkUMeStdChxi.htm)|Spell Effect: Animal Feature (Owl Eyes)|Effet : Trait animal (Yeux de hibou)|changé|
|[02-OplbQWW6i1NtMq9f.htm](spell-effects/02-OplbQWW6i1NtMq9f.htm)|Spell Effect: Ki Strike (Good)|Effet : Frappe ki (Bien)|changé|
|[02-ptOqsN5FS0nQh7RW.htm](spell-effects/02-ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|Effet : Forme animale (Félin)|changé|
|[02-qPaEEhczUWCQo6ux.htm](spell-effects/02-qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|Effet : Forme animale (Requin)|changé|
|[02-sN3mQ7YrPBogEJRn.htm](spell-effects/02-sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|Effet : Forme animale (Canidé)|changé|
|[02-sPCWrhUHqlbGhYSD.htm](spell-effects/02-sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|Effet : Agrandissement|changé|
|[02-tk3go5Cl6Qt130Dk.htm](spell-effects/02-tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|Effet : Forme animale (Singe)|changé|
|[03-3zdBGENpmaze1bpq.htm](spell-effects/03-3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|Effet: Forme de vase (Cube gélatineux)|changé|
|[03-6IvTWcispcDaw88N.htm](spell-effects/03-6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|Effet : Forme d'insecte (Fourmi)|changé|
|[03-782NyomkDHyfsUn6.htm](spell-effects/03-782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Effet : Forme d'insecte (Araignée)|changé|
|[03-7tfF8ifVvOKNud8t.htm](spell-effects/03-7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|Effet: Form de vase (Vase grise)|changé|
|[03-amTa9jSml9ioKduN.htm](spell-effects/03-amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|Effet : Forme d'insecte (Scarabée)|changé|
|[03-bOjuEX3qj7XAOoDF.htm](spell-effects/03-bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Effet : Forme d'insecte (Scorpion)|changé|
|[03-D0Qj5tC1hGUjzQc4.htm](spell-effects/03-D0Qj5tC1hGUjzQc4.htm)|Spell Effect: Elemental Motion (Water)|Effet: Mobilité élémentaire (Eau)|changé|
|[03-DENMzySYANjUBs4O.htm](spell-effects/03-DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Effet : Forme d'insecte (Mille-pattes)|changé|
|[03-EKdqKCuyWSkpXpyJ.htm](spell-effects/03-EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|Effet: Forme de vase (Pudding noir)|changé|
|[03-iqtjMVl6rGQhX2k8.htm](spell-effects/03-iqtjMVl6rGQhX2k8.htm)|Spell Effect: Elemental Motion (Air)|Effet: Mobilité élémentaire(Air)|changé|
|[03-iZYjxY0qYvg5yPP3.htm](spell-effects/03-iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|Effet: Ailes d'ange|changé|
|[03-l9HRQggofFGIxEse.htm](spell-effects/03-l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|Effet : Héroïsme|changé|
|[03-llrOM8rPP9nxIuEN.htm](spell-effects/03-llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|Effet : Forme d'insecte (Mante)|changé|
|[03-PpkOZVoHkBZUmddx.htm](spell-effects/03-PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|{Effet: Forme de vase (Gelée ocre)|changé|
|[03-q4EEYltjqpRGiLsP.htm](spell-effects/03-q4EEYltjqpRGiLsP.htm)|Spell Effect: Elemental Motion (Fire)|Effet: Mobilité élémentaire (Feu)|changé|
|[03-qbOpis7pIkXJbM2B.htm](spell-effects/03-qbOpis7pIkXJbM2B.htm)|Spell Effect: Elemental Motion (Earth)|Effet: Mobilité élémentaire (Terre)|changé|
|[04-0Cyf07wboRp4CmcQ.htm](spell-effects/04-0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|Effet : Forme de dinosaure (Ankylosaure)|changé|
|[04-0QVufU5o3xIxiHmP.htm](spell-effects/04-0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|Effet : Forme aérienne (Oiseau)|changé|
|[04-0s6YaL3IjqECmjab.htm](spell-effects/04-0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Wyrm|Effet : Rugissement du Ver|changé|
|[04-14AFzcwkN019dzcl.htm](spell-effects/04-14AFzcwkN019dzcl.htm)|Spell Effect: Shifting Form (Claws)|Effet: Forme changeant (Griffes)|changé|
|[04-1VuHjj32wge2gPOr.htm](spell-effects/04-1VuHjj32wge2gPOr.htm)|Spell Effect: Animal Feature (Wings)|Effet : Trait animal (Ailes)|changé|
|[04-3HEiYVhqypfc4IsP.htm](spell-effects/04-3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|Effet : Secret bien gardé|changé|
|[04-41WThj17MZBXTO2X.htm](spell-effects/04-41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|Effet : Agrandissement (Intensifié 4e)|changé|
|[04-542Keo6txtq7uvqe.htm](spell-effects/04-542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|Effet  Forme de dinosaure (Tyrannosaure)|changé|
|[04-8GUkKvCeI0xljCOk.htm](spell-effects/04-8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|Effet : Vol de l'ouragan|changé|
|[04-A48jNUOAmCljx8Ru.htm](spell-effects/04-A48jNUOAmCljx8Ru.htm)|Spell Effect: Shifting Form (Darkvision)|Effet: Forme changeante (Vision dans le noir)|changé|
|[04-AJkRUIdYLnt4QOOg.htm](spell-effects/04-AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|Effet : Tenter le sort|changé|
|[04-byXkHIKFwuKrZ55M.htm](spell-effects/04-byXkHIKFwuKrZ55M.htm)|Spell Effect: Shifting Form (Scent)|Effet: Forme changeante (Odorat)|changé|
|[04-iJ7TVW5tDnZG9DG8.htm](spell-effects/04-iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|Effet : Avantage du compétiteur|changé|
|[04-iOKhr2El8R6cz6YI.htm](spell-effects/04-iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Effet : Forme de dinosaure (Tricératops)|changé|
|[04-j6po934p4jcUVC6l.htm](spell-effects/04-j6po934p4jcUVC6l.htm)|Spell Effect: Shifting Form (Speed)|Effet: Forme changeante (Vitesse)|changé|
|[04-jvwKRHtOiPAm4uAP.htm](spell-effects/04-jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|Effet : Forme aérienne (Chauve-souris)|changé|
|[04-KkDRRDuycXwKPa6n.htm](spell-effects/04-KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|Effet : Forme de dinosaure (Brontosaure)|changé|
|[04-nbW4udOUTrCGL3Gf.htm](spell-effects/04-nbW4udOUTrCGL3Gf.htm)|Spell Effect: Shifting Form (Climb Speed)|Effet: Forme changeante (Escalade)|changé|
|[04-nHXKK4pRXAzrLdEP.htm](spell-effects/04-nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take Its Course (Affliction, Help)|Effet : Suivre son cours (Afflicton, aide)|changé|
|[04-oJbcmpBSHwmx6FD4.htm](spell-effects/04-oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|Effet : Forme de dinosaure (Deinonychus)|changé|
|[04-R27azQfzeFuFc48G.htm](spell-effects/04-R27azQfzeFuFc48G.htm)|Spell Effect: Take Its Course (Affliction, Hinder)|Effet : Suivre son cours (Affliction, entraver)|changé|
|[04-T6XnxvsgvvOrpien.htm](spell-effects/04-T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|Effet : Forme de Dinosaure (Stégosaure)|changé|
|[04-UH2sT6eW5e31Xytd.htm](spell-effects/04-UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|Effet : Défi respectueux|changé|
|[04-UjoNm3lrhlg4ctAQ.htm](spell-effects/04-UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|Effet : Forme aérienne (Ptérosaure)|changé|
|[04-Um25D1qLtZWOSBny.htm](spell-effects/04-Um25D1qLtZWOSBny.htm)|Spell Effect: Shifting Form (Swim Speed)|Effet: Forme changeante (Nage)|changé|
|[04-xgZxYqjDPNtsQ3Qp.htm](spell-effects/04-xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|Effet : Forme aérienne (Guêpe)|changé|
|[04-Xlwt1wpjEKWBLUjK.htm](spell-effects/04-Xlwt1wpjEKWBLUjK.htm)|Spell Effect: Animal Feature (Fish Tail)|Effet : Trait animal (Queue de poisson)|changé|
|[05-14m4s0FeRSqRlHwL.htm](spell-effects/05-14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|Effet: Contre mesures arcaniques|changé|
|[05-51M5Qblm1TDTq7HB.htm](spell-effects/05-51M5Qblm1TDTq7HB.htm)|Spell Effect: Blessing of Defiance (2 Action Reflex)|Effet: Bénédiction du défi (Réflexes à 2 actions)|changé|
|[05-5MI2c9IgxfSeGZQo.htm](spell-effects/05-5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|Effet : Saut du vent|changé|
|[05-8eWLR0WCf5258z8X.htm](spell-effects/05-8eWLR0WCf5258z8X.htm)|Spell Effect: Elemental Form (Earth)|Effet : Forme élémentaire (Terre)|changé|
|[05-A61eVVVyUuaUl3tz.htm](spell-effects/05-A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|Effet: Marque céleste|changé|
|[05-ceEA7nBGNmoR8Sjj.htm](spell-effects/05-ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|Effet : Litanie d'égoïsme|changé|
|[05-cTBYHfiXDOA09G4b.htm](spell-effects/05-cTBYHfiXDOA09G4b.htm)|Spell Effect: Traveler's Transit (Fly)|Effet : Voyageur en transit (Vol)|changé|
|[05-DliizYpHcmBG130w.htm](spell-effects/05-DliizYpHcmBG130w.htm)|Spell Effect: Elemental Form (Air)|Effet : Forme élémentaire (Air)|changé|
|[05-fIloZhZVH1xTnX4B.htm](spell-effects/05-fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|Effet : Forme de plante (Grand tertre)|changé|
|[05-JrNHFNxJayevlv2G.htm](spell-effects/05-JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|Effet : Forme de plante (Attrape-mouches)|changé|
|[05-Jw8C5RUaxQ9N7VUH.htm](spell-effects/05-Jw8C5RUaxQ9N7VUH.htm)|Spell Effect: Blessing of Defiance (Will)|Effet: Bénédiction de défi (Volonté)|changé|
|[05-jy4edd6pvJvJgOSP.htm](spell-effects/05-jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings (Own Speed)|Effet: Ailes de dragon (Sa vitesse)|changé|
|[05-kxMBdANwCcF841uA.htm](spell-effects/05-kxMBdANwCcF841uA.htm)|Spell Effect: Elemental Form (Water)|Effet : Forme élémentaire (Eau)|changé|
|[05-LMzFBnOEPzDGzHg4.htm](spell-effects/05-LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|Effet: Anatomie étrange|changé|
|[05-lWYIwbFhw0SWVLaP.htm](spell-effects/05-lWYIwbFhw0SWVLaP.htm)|Spell Effect: Blessing of Defiance (Fortitude)|Effet: Bénédiction de défi (Vigueur)|changé|
|[05-ML9xn5YKPID8gODV.htm](spell-effects/05-ML9xn5YKPID8gODV.htm)|Spell Effect: Blessing of Defiance (Reflex)|Effet: Bénédiction de défi (Réflexes)|changé|
|[05-phIoucsDa3iplMm2.htm](spell-effects/05-phIoucsDa3iplMm2.htm)|Spell Effect: Elemental Form (Fire)|Effet : Forme élémentaire (Feu)|changé|
|[05-tu8FyCtmL3YYR2jL.htm](spell-effects/05-tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|Effet : Forme de plante (Arboréen)|changé|
|[05-V7jAnItnVqtfCAKt.htm](spell-effects/05-V7jAnItnVqtfCAKt.htm)|Spell Effect: Dragon Wings (60 Feet)|Effet: Ailes de dragon (18 mètres)|changé|
|[06-8aNZhlkzRTRKlKag.htm](spell-effects/06-8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Gold)|Effet : Forme de dragon (Or)|changé|
|[06-b5OyBdc0bolgWZZT.htm](spell-effects/06-b5OyBdc0bolgWZZT.htm)|Spell Effect: Tempest Form (Air)|Effet : Forme tempêtueuse (Air)|changé|
|[06-C3RdbEQTvawqKAhw.htm](spell-effects/06-C3RdbEQTvawqKAhw.htm)|Spell Effect: Tempest Form (Water)|Effet : Forme tempêtueuse (Eau)|changé|
|[06-DrNpuMj14wVj4bWF.htm](spell-effects/06-DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Copper)|Effet : Forme de dragon (Cuivre)|changé|
|[06-ETgzIIv3M2zvclAR.htm](spell-effects/06-ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Blue)|Effet : Forme de dragon (Bleu)|changé|
|[06-H6ndYYYlADWwqVQb.htm](spell-effects/06-H6ndYYYlADWwqVQb.htm)|Spell Effect: Dragon Form (White)|Effet : Forme de dragon (Blanc)|changé|
|[06-HDKJAUXMbtxnBdgR.htm](spell-effects/06-HDKJAUXMbtxnBdgR.htm)|Spell Effect: Tempest Form (Mist)|Effet : Forme tempêtueuse (Brume)|changé|
|[06-jtW3VfI5Kktuy3GH.htm](spell-effects/06-jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Bronze)|Effet : Forme de dragon (Bronze)|changé|
|[06-l8HkOKfiUqd3BUwT.htm](spell-effects/06-l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|Effet : Forme ancestrale|changé|
|[06-lIl0yYdS9zojOZhe.htm](spell-effects/06-lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|Effet : Forme génératrice de vie|changé|
|[06-nWEx5kpkE8YlBZvy.htm](spell-effects/06-nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Green)|Effet : Forme de dragon (Vert)|changé|
|[06-OeCn76SB92GPOZwr.htm](spell-effects/06-OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Brass)|Effet : Forme de dragon (Airain)|changé|
|[06-rHXOZAFBdRXIlxt5.htm](spell-effects/06-rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Black)|Effet : Forme de dragon (Noir)|changé|
|[06-TUyEeLyqdJL6PwbH.htm](spell-effects/06-TUyEeLyqdJL6PwbH.htm)|Spell Effect: Dragon Form (Silver)|Effet : Forme de dragon (Argent)|changé|
|[06-V4a9pZHNUlddAwTA.htm](spell-effects/06-V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Red)|Effet : Forme de dragon (Rouge)|changé|
|[06-W4lb3417rNDd9tCq.htm](spell-effects/06-W4lb3417rNDd9tCq.htm)|Spell Effect: Righteous Might|Effet: Force du molosse|changé|
|[07-2Ss5VblfZNHg1HjN.htm](spell-effects/07-2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|Effet: Forme cosmique (Soleil)|changé|
|[07-AnawxScxqUiRuGTm.htm](spell-effects/07-AnawxScxqUiRuGTm.htm)|Spell Effect: Divine Vessel 9th level (Evil)|Effet: Réceptacle divin (Mauvais)|changé|
|[07-dIftJU6Ki2QSLCOD.htm](spell-effects/07-dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel 9th level (Chaotic)|Effet: Réceptacle divin Niveau 9 (Chaotique)}|changé|
|[07-evK8JR3j2iWGWaug.htm](spell-effects/07-evK8JR3j2iWGWaug.htm)|Spell Effect: Divine Vessel (Evil)|Effet: Réceptacle divin (Mauvais)|changé|
|[07-GDzn5DToE62ZOTrP.htm](spell-effects/07-GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel (Chaotic)|Effet: Réceptacle divin (Chaotique)|changé|
|[07-KMF2t3qqzyFP0rxL.htm](spell-effects/07-KMF2t3qqzyFP0rxL.htm)|Spell Effect: Divine Vessel (Good)|Effet: Réceptacle divin (Bon)|changé|
|[07-OxJEUhim6xzsHIyi.htm](spell-effects/07-OxJEUhim6xzsHIyi.htm)|Spell Effect: Divine Vessel 9th level (Good)|Effet: Réceptacle divin (Bon)|changé|
|[07-tBgwWblDp1xdxN4D.htm](spell-effects/07-tBgwWblDp1xdxN4D.htm)|Spell Effect: Divine Vessel (Lawful)|Effet: Réceptacle divin (Loyal)|changé|
|[07-tfdDpf9xSWgQer5g.htm](spell-effects/07-tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|Effet: Forme cosmique (Lune)|changé|
|[07-wKdIf5x7zztiFHpL.htm](spell-effects/07-wKdIf5x7zztiFHpL.htm)|Spell Effect: Divine Vessel 9th level (Lawful)|Effet: Réceptacle divin Niveau 9 (Loyal)|changé|
|[08-b8bfWIICHOsGVzjp.htm](spell-effects/08-b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|Effet : Forme monstrueuse (Phénix)|changé|
|[08-Eik8Fj8nGo2GLcbn.htm](spell-effects/08-Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|Effet : Forme monstrueuse (Serpent de mer)|changé|
|[08-rEsgDhunQ5Yx8KZx.htm](spell-effects/08-rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Purple Worm)|Effet : Forme monstrueuse (Ver pourpre)|changé|
|[09-1p5LEpMSRFG1099t.htm](spell-effects/09-1p5LEpMSRFG1099t.htm)|Spell Effect: Ki Form (Lawful)|Effet : Forme ki (Loi)|changé|
|[09-bbOdbGiRXGEQ2aY0.htm](spell-effects/09-bbOdbGiRXGEQ2aY0.htm)|Spell Effect: Ki Form (Positive)|Effet : Forme ki (Positif)|changé|
|[09-F0XnusshRzgMdFT1.htm](spell-effects/09-F0XnusshRzgMdFT1.htm)|Spell Effect: Ki Form (Force)|Effet : Forme ki (Force)|changé|
|[09-h28nZ8JB5JBwr87U.htm](spell-effects/09-h28nZ8JB5JBwr87U.htm)|Spell Effect: Ki Form (Negative)|Effet : Forme ki (Négatif)|changé|
|[09-Mp7252yAsSA8lCEA.htm](spell-effects/09-Mp7252yAsSA8lCEA.htm)|Spell Effect: One With the Land|Effet: Uni à la terre|changé|
|[10-blBXnWb1Y8q8YYMh.htm](spell-effects/10-blBXnWb1Y8q8YYMh.htm)|Spell Effect: Primal Summons (Fire)|Effet : Convocations primordiales (Feu)|changé|
|[10-fwaAe71qfnK7SiOB.htm](spell-effects/10-fwaAe71qfnK7SiOB.htm)|Spell Effect: Primal Summons (Air)|Effet : Convocations primordiales (Air)|changé|
|[10-NXzo2kdgVixIZ2T1.htm](spell-effects/10-NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|Effet : Compagnon alpha|changé|
|[10-TAAWbJgfESltn2we.htm](spell-effects/10-TAAWbJgfESltn2we.htm)|Spell Effect: Primal Summons (Water)|Effet : Convocations primordiales (Eau)|changé|
|[10-Xl48OsJ47oDVZAVQ.htm](spell-effects/10-Xl48OsJ47oDVZAVQ.htm)|Spell Effect: Primal Summons (Earth)|Effet : Convocations primordiales (Terre)|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01-0bfqYkNaWsdTmtrc.htm](spell-effects/01-0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|Effet : Compagnon juvénile|libre|
|[01-0o984LjzIFXxeXIF.htm](spell-effects/01-0o984LjzIFXxeXIF.htm)|Spell Effect: Evolution Surge (Amphibious)|Effet: Flux d'évolution (Amphibie)|libre|
|[01-0OC945wcZ4H4akLz.htm](spell-effects/01-0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|Effet: Visage du conjurateur|libre|
|[01-0yy4t4UY1HqrEo70.htm](spell-effects/01-0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|Effet: Forme de diable (Barbazu)|libre|
|[01-4VOZP2ArmS12nvz8.htm](spell-effects/01-4VOZP2ArmS12nvz8.htm)|Spell Effect: Draw Ire (Critical Failure)|Effet: Attirer la colère (Échec critique)|libre|
|[01-5R3ewWLFkgqTvZsc.htm](spell-effects/01-5R3ewWLFkgqTvZsc.htm)|Spell Effect: Elemental Gift (Fire)|Effet: Don élémentaire (Feu)|libre|
|[01-6Ev2ytJZfr2t33iy.htm](spell-effects/01-6Ev2ytJZfr2t33iy.htm)|Spell Effect: Evolution Surge (Sight)|Effet: Flux d'évolution (Vision)|libre|
|[01-6qvLnIkWAoGvTIWy.htm](spell-effects/01-6qvLnIkWAoGvTIWy.htm)|Spell Effect: Community Repair (Critical Failure)|Effet: Réparation communautaire (Échec critique)|libre|
|[01-7zJPd2BsFl82qFRV.htm](spell-effects/01-7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression (+3)|Effet: Agression protectrice (+3)|libre|
|[01-8ecGfjmxnBY3WWao.htm](spell-effects/01-8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|Effet: Multitude de couteaux|libre|
|[01-9yzlmYUdvdQshTDF.htm](spell-effects/01-9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|Effet : Porte-voix|libre|
|[01-a3uZckqOY9zQWzZ2.htm](spell-effects/01-a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|Effet: Lecture de l'air|libre|
|[01-BDMEqBsumguTrMXa.htm](spell-effects/01-BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|Effet: Forme de diable (Érinye)|libre|
|[01-BsGZdgiEElNlgZVv.htm](spell-effects/01-BsGZdgiEElNlgZVv.htm)|Spell Effect: Draw Ire (Failure)|Effet: Attirer la colère (Échec)|libre|
|[01-CunoTO00M6eZjoFm.htm](spell-effects/01-CunoTO00M6eZjoFm.htm)|Spell Effect: Impeccable Flow (8th)|Effet: Flux impeccable (8e)|libre|
|[01-cwetyC5o4dRyFWJZ.htm](spell-effects/01-cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|Effet: Générosité du nécromant|libre|
|[01-F10ofwC0k1ELIaV4.htm](spell-effects/01-F10ofwC0k1ELIaV4.htm)|Spell Effect: Impeccable Flow (Critical Failure Effect)|Effet: Flux impeccable (Effet d'échec critique)|libre|
|[01-fEhCbATDNlt6c1Ug.htm](spell-effects/01-fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|Effet: Extraction du poison|libre|
|[01-fKeZDm8kpDFK5HWp.htm](spell-effects/01-fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|Effet: Forme de diable (Sarglagon)|libre|
|[01-fLOQMycP5tmXgPv9.htm](spell-effects/01-fLOQMycP5tmXgPv9.htm)|Spell Effect: Elemental Gift (Earth)|Effet: Don élémentaire (Terre)|libre|
|[01-FR4ucNi2ceHZdrpB.htm](spell-effects/01-FR4ucNi2ceHZdrpB.htm)|Spell Effect: Warding Aggression (+1)|Effet: Agression protectrice (+1)|libre|
|[01-gmgXUazwnm0i305Z.htm](spell-effects/01-gmgXUazwnm0i305Z.htm)|Spell Effect: Infectious Enthusiasm (Attack)|Effet: Enthousiasme communicatif (Attaque)|libre|
|[01-HEbbxKtBzsLhFead.htm](spell-effects/01-HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|Effet: Forme de diable (Osyluth)|libre|
|[01-hkLhZsH3T6jc9S1y.htm](spell-effects/01-hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|Effet: Voile de rêves|libre|
|[01-i9YITDcrq1nKjV5l.htm](spell-effects/01-i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody|Effet: Mélodie infectieuse|libre|
|[01-jx5wWd8eNw6swFa1.htm](spell-effects/01-jx5wWd8eNw6swFa1.htm)|Spell Effect: Infectious Enthusiasm (Will Saves)|Effet: Enthousiasme communicatif (Jets de Volonté)|libre|
|[01-lTL5VwNrZ5xiitGV.htm](spell-effects/01-lTL5VwNrZ5xiitGV.htm)|Spell effect: Nudge the Odds|Effet: Renforcer les chances|libre|
|[01-m4ZU6BerQmEcrEun.htm](spell-effects/01-m4ZU6BerQmEcrEun.htm)|Spell Effect: Impeccable Flow (2nd)|Effet: Flux impeccable (2e)|libre|
|[01-n6NK7wqhTxWr3ij8.htm](spell-effects/01-n6NK7wqhTxWr3ij8.htm)|Spell Effect: Warding Aggression (+2)|Effet: Agression protectrice (+2)|libre|
|[01-ndj0TpLxyzbyzcm4.htm](spell-effects/01-ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|Effet: Nécroser (Jambes)|libre|
|[01-nIryhRgeiacQw1Em.htm](spell-effects/01-nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|Effet: Bourgeons apaisants|libre|
|[01-p10GOTCXqeJp4wzg.htm](spell-effects/01-p10GOTCXqeJp4wzg.htm)|Spell Effect: Infectious Enthusiasm (Charisma Based)|Effet: Enthousiasme communicatif (tests basés sur le charisme)|libre|
|[01-PDoTV4EhJp63FEaG.htm](spell-effects/01-PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|Effet : Attirer la colère (Succès)|libre|
|[01-pPMldkAbPVOSOPIF.htm](spell-effects/01-pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|Effet: Protéger le compagnon|libre|
|[01-R3j6song8sYLY5vG.htm](spell-effects/01-R3j6song8sYLY5vG.htm)|Spell Effect: Community Repair (Critical Success)|Effet: Réparation communautaire (Succès critique)|libre|
|[01-TjGHxli0edXI6rAg.htm](spell-effects/01-TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|Effet: Joie malsaine (Succès)|libre|
|[01-u04C6DgBFP9PKzDx.htm](spell-effects/01-u04C6DgBFP9PKzDx.htm)|Spell Effect: Impeccable Flow (5th)|Effet: Flux impeccable (5e)|libre|
|[01-UVrEe0nukiSmiwfF.htm](spell-effects/01-UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|Effet: Renforcer l'eidolon|libre|
|[01-XMOWgJV8UXWmpgk0.htm](spell-effects/01-XMOWgJV8UXWmpgk0.htm)|Spell Effect: Evolution Surge (Speed)|Effet: Flux d'évolution (Vitesse)|libre|
|[01-yke7fAUDxUNzouQc.htm](spell-effects/01-yke7fAUDxUNzouQc.htm)|Spell Effect: Elemental Gift (Air)|Effet: Don élémentaire (Air)|libre|
|[01-yq6iu0Qxg3YEbb6s.htm](spell-effects/01-yq6iu0Qxg3YEbb6s.htm)|Spell Effect: Elemental Gift (Water)|Effet: Don élémentaire (Eau)|libre|
|[02-0q2716S34XL1y9Hh.htm](spell-effects/02-0q2716S34XL1y9Hh.htm)|Spell Effect: Rapid Adaptation (Underground)|Effet: Adaptation rapide (Souterrain)|libre|
|[02-81TfqzTfIqkQA4Dy.htm](spell-effects/02-81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|Effet: Domination tonitruante|libre|
|[02-AM49w68oKykc2fHI.htm](spell-effects/02-AM49w68oKykc2fHI.htm)|Spell Effect: Rapid Adaptation (Plains)|Effet: Adaptation rapide (Plaines)|libre|
|[02-ei9MIyZbIaP4AZmh.htm](spell-effects/02-ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|Effet: Feu follet enflammé|libre|
|[02-fGK6zJ7mWz9D5QYo.htm](spell-effects/02-fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation (Aquatic Base Swim Speed)|Effet: Adaptation radide (Aquatique : Base de nage)|libre|
|[02-iiV80Kexj6vPmzqU.htm](spell-effects/02-iiV80Kexj6vPmzqU.htm)|Spell Effect: Rapid Adaptation (Arctic)|Effet: Adaptation rapide (Arctique)|libre|
|[02-itmiGioGNuVvt4QE.htm](spell-effects/02-itmiGioGNuVvt4QE.htm)|Spell Effect: Rapid Adaptation (Aquatic Speed Bonus)|Effet : Adaptation rapide (Aquatique: bonus vitesse de nage)|libre|
|[02-IXS15IQXYCZ8vsmX.htm](spell-effects/02-IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|Effet: Vision dans le noir|libre|
|[02-jtMo6qS47hPx6EbR.htm](spell-effects/02-jtMo6qS47hPx6EbR.htm)|Spell Effect: Rapid Adaptation (Forest)|Effet: Adaptation rapide (Forêt)|libre|
|[02-qo7DoF11Xl9gqmFc.htm](spell-effects/02-qo7DoF11Xl9gqmFc.htm)|Spell Effect: Rapid Adaptation (Desert)|Effet: Adaptation rapide (Désert)|libre|
|[02-sDN9b4bjCGH2nQnG.htm](spell-effects/02-sDN9b4bjCGH2nQnG.htm)|Spell Effect: Rapid Adaptation (Mountain)|Effet: Adaptation rapide (Montagne)|libre|
|[02-xPVOvWNJORvm8EwP.htm](spell-effects/02-xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|Effet: Imiter un mort-vivant|libre|
|[03-czteoX2cggQzfkK9.htm](spell-effects/03-czteoX2cggQzfkK9.htm)|Spell Effect: Evolution Surge (Climb)|Effet: Flux d'évolution (Escalade)|libre|
|[03-PhBrHvBwvq8rni9C.htm](spell-effects/03-PhBrHvBwvq8rni9C.htm)|Spell Effect: Evolution Surge (Large)|Effet: Flux d'évolution (Grande)|libre|
|[03-Uj9VFXoVMH0mTTdt.htm](spell-effects/03-Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|Effet: Vision des organes|libre|
|[03-zPGVOLz6xhsQN35C.htm](spell-effects/03-zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|Effet: Compagnon venimeux|libre|
|[04-28NvrpZmELvyrHUt.htm](spell-effects/04-28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|Effet: Gravité variable (Gravité élevée)|libre|
|[04-c4cIfS2974nUJDPt.htm](spell-effects/04-c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|Effet: Forme de fée (dryade)|libre|
|[04-DwM5qcFp4JgKhXrY.htm](spell-effects/04-DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|Effet: Forme de fée (Licorne)|libre|
|[04-fCIT9YgGUwIc3Z9G.htm](spell-effects/04-fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|Effet: Attirer la foudre|libre|
|[04-heAj9paC8ZRh7QEj.htm](spell-effects/04-heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|Effet: Forme de fée (Bonnet rouge)|libre|
|[04-KcBqo33ekJHxZLHo.htm](spell-effects/04-KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|Effet: Forme de fée (Naïade)|libre|
|[04-S75DOLjKaSJGMc0D.htm](spell-effects/04-S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|Effet: Forme de fée (Élananxe)|libre|
|[05-1gTnFmfLUQnCo3TK.htm](spell-effects/05-1gTnFmfLUQnCo3TK.htm)|Spell Effect: Dread Ambience (Success)|Effet: Ambiance terrifiante (Succès)|libre|
|[05-4FD4vJqVpuuJjk9Q.htm](spell-effects/05-4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|Effet: Échange d'esprit (Succès critique)|libre|
|[05-4Lt69zSeYElEfDrZ.htm](spell-effects/05-4Lt69zSeYElEfDrZ.htm)|Spell Effect: Dread Ambience (Critical Failure)|Effet: Ambiance terrifiante (Échec critique)|libre|
|[05-6SG8wVmppv4oXZtx.htm](spell-effects/05-6SG8wVmppv4oXZtx.htm)|Spell Effect: Mantle of the Magma Heart (Fiery Grasp)|Manteau du cœur magmatique (Poigne enflammée)|libre|
|[05-FT5Tt2DKBRutDqbV.htm](spell-effects/05-FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience (Critical Success)|Effet: Ambiance terrifiante (Succès critique)|libre|
|[05-gKGErrsS1WoAyWub.htm](spell-effects/05-gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|Effet: Forme d'aberration (Gogiteth)|libre|
|[05-ib58LaffEUIypuzL.htm](spell-effects/05-ib58LaffEUIypuzL.htm)|Spell Effect: Evolution Surge (Huge)|Effet: Flux d'évolution (Très grand)|libre|
|[05-inNfTmtWpsxeGBI9.htm](spell-effects/05-inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|Effet: Vision dans le noir (24 heures)|libre|
|[05-qRyynMOflGajgAR3.htm](spell-effects/05-qRyynMOflGajgAR3.htm)|Spell Effect: Mantle of the Magma Heart (Enlarging Eruption)|Effet: Manteau du cœur magmatique (Éruption magmatique)|libre|
|[05-sfJyQKmoxSRo6FyP.htm](spell-effects/05-sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|Effet: Forme d'aberration (Gug)|libre|
|[05-SjfDoeymtnYKoGUD.htm](spell-effects/05-SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|Effet: Forme d'aberration (Otyugh)|libre|
|[05-u0AznkjTZAVnCMNp.htm](spell-effects/05-u0AznkjTZAVnCMNp.htm)|Spell Effect: Evolution Surge (Fly)|Effet: Flux d'évolution (Vol)|libre|
|[05-wqh8D9kHGItZBvtQ.htm](spell-effects/05-wqh8D9kHGItZBvtQ.htm)|Spell Effect: Mantle of the Frozen Heart (Icy Claws)|Effet: Manteau du cœur gelé (Griffes de glace)|libre|
|[05-xsy1yaCj0SVsn502.htm](spell-effects/05-xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|Effet: Forme d'aberration (Chuul)|libre|
|[05-YCno88Te0nfwFgVo.htm](spell-effects/05-YCno88Te0nfwFgVo.htm)|Spell Effect: Mantle of the Frozen Heart (Ice Glide)|Effet: Manteau du cœur gelé (Glissement glacé)|libre|
|[06-0W87OkYi3qCwNGSj.htm](spell-effects/06-0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|Effet: Forme de daémon (Piscodaémon)|libre|
|[06-1n84AqLtsdT8I64W.htm](spell-effects/06-1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|Effet: Forme de daémon (Ceustodaémon)|libre|
|[06-20egTKICPMhibqgn.htm](spell-effects/06-20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|Effet: Forme de démon (Vrock)|libre|
|[06-hnfQyf05IIa7WPBB.htm](spell-effects/06-hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|Effet: Forme de démon (Nabasu)|libre|
|[06-ScF0ECWnfXMHYLDL.htm](spell-effects/06-ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|Effet: Forme de daémon (Leukodaémon)|libre|
|[06-sXe7cPazOJbX41GU.htm](spell-effects/06-sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|Effet: Forme de démon (Hezrou)|libre|
|[06-X1kkbRrh4zJuDGjl.htm](spell-effects/06-X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|Effet: Forme de démon (Babau|libre|
|[06-zIRnnuj4lARq43DA.htm](spell-effects/06-zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|Effet: Forme de daémon (Méladaémon)|libre|
|[07-3vWfew0TIrcGRjLZ.htm](spell-effects/07-3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|Effet: Forme d'ange (Deva monadique)|libre|
|[07-Bd86oAvK3RLN076H.htm](spell-effects/07-Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|Effet: Forme d'ange (Deva movanique)}|libre|
|[07-sccNh8j1PKLHCKh1.htm](spell-effects/07-sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|Effet: Forme d'ange (Choral)|libre|
|[07-WEpgIGFwtRb3ef1x.htm](spell-effects/07-WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|Effet: Forme d'ange (Balisse)|libre|
|[08-kMoOWWBqDYmPcYyS.htm](spell-effects/08-kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|Effet: Baignade de sang|libre|
